const Mocker = require('./src/mock')

module.exports = {
  devServer: {
    port: 8080,
    before: Mocker
  }
}

