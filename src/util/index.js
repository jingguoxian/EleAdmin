
const Util = {
  uuid: () => {
    // 创建空对象，并获取该对象的URL对象
    let url = URL.createObjectURL(new Blob())
    // 获取字符串
    // blob:https://xxx.com/b250d159-e1b6-4a87-9002-885d90033be3
    let uuid = url.toString()
    // 释放空对象
    URL.revokeObjectURL(url);
    return uuid.substr(uuid.lastIndexOf("/") + 1)
  },

  isDisabled(obj, values) {
    if (obj.disabled) return true

    if (!values || !obj.when  || !obj.when.name) return false

    const { name, /** @type { boolean|array|string|number } */value = false }= obj.when

    const v = values[name]

    return !(Array.isArray(value) ? value.includes(v) : value === v)
  }
}

export default  Util
