import YmBlack from './layouts/YmBlack'
import YmRouter from './views/YmRouter'

const components = [
  {
    name: 'YmMain',
    path: 'layouts/YmMain.vue'
  },
  {
    name: 'YmMenu',
    path: 'views/YmMenu.vue'
  },
  {
    name: 'YmCurd',
    path: 'views/YmCurd.vue'
  },
  {
    name: 'YmFormView',
    path: 'views/YmFormView.vue'
  },
  {
    name: 'YmChart',
    path: 'views/YmChart.vue'
  },
  {
    name: 'YmTable',
    path: 'components/YmTable.vue'
  },
  {
    name: 'YmButton',
    path: 'components/YmButton.vue'
  },
  {
    name: 'YmToolbar',
    path: 'components/YmToolbar.vue'
  },
  {
    name: 'YmFormFields',
    path: 'components/form/YmFormFields.vue'
  },
  {
    name: 'YmForm',
    path: 'components/form/YmForm.vue'
  },
  {
    name: 'YmPair',
    path: 'components/form/YmPair.vue'
  },
  {
    name: 'YmFileUploader',
    path: 'components/form/YmFileUploader.vue'
  },
  {
    name: 'YmRelation',
    path: 'components/form/YmRelation.vue'
  },
  {
    name: 'YmMaker',
    path: 'maker/YmMaker.vue'
  }
]

export const Ym = {
  install: function (Vue) {
    if (this.installed) return
    this.installed = true

    Vue.component(YmBlack.name, YmBlack)
    Vue.component(YmRouter.name, YmRouter)

    components.forEach((component) => {
      Vue.component(
        component.name,
        () => import('@/ym/' + component.path)
      )
    })
  }
}

export default Ym
