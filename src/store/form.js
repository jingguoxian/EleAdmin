/**
 * 表单定义
 */
import { DFT, ICON, LABEL, WIDGET } from "@/store/elementui";
import YmAvatarUploader from "@/ym/components/form/YmAvatarUploader";

/**
 * @typedef { function (h, props: object): VNode[] } onCreate
 */

/**
 * @typedef { object } Widget
 *
 * @property { string } [TAG] tag
 * @property { string } [LABEL] 标签
 * @property { string } [ICON] 缩略图标
 * @property { onCreate } [ON_CREATE] onCreate钩子
 * @property { function () : (number|string|boolean|array|{}) } [DFT] 默认值
 *
 */
/**
 * @typedef { object } Form
 *
 * @property { string } [LABEL]
 * @property { string } [NAME]
 *
 * @property { Array<Form~Field> } [FIELDS] 域列表
 * @property { string } [LABEL_WIDTH] 标签宽度
 * @property { "left"|"right"|"center" } [LABEL_POSITION] 标签位置
 * @property { ElementUI~Button|string } [SUBMIT] 提交按钮定义
 * @property { Toolbar } [ElementUI~TOOLBAR]
 *
 * @property {{}} [ROW] 数据
 */

/**
 * @typedef { object } Form~Field
 *
 * @property { string } [LABEL]
 * @property { string } [NAME]
 *
 * @property { string } [WIDGET] 组件类型
 * @property { number } [COL] 域宽度
 *
 * @property { [] } [rules] 校验信息
 * @property { Array<Form~Field~Option> } [OPTIONS] 选项列表
 *
 */

/**
 * @typedef { { value: number | string, label: string } } Form~Field~Option
 */

// 表单定义
export const FORM            = 'form'
/**
 * 表单宽度
 */
export const WIDTH           = 'width'
/**
 * 表单域列表
 */
export const FIELDS          = 'fields'
/**
 * 提交按钮 { url | Button }
 */
export const SUBMIT          = 'submit'
/**
 * 标签宽度
 */
export const LABEL_WIDTH     = 'label-width'
/**
 * 标签定义
 */
export const LABEL_POSITION  = 'label-position'

// 域样式定义
/**
 * 宽度
 */
export const COL             = 'col'

// widget定义
export const INPUT_FIELD     = 'input-field'
export const TEXT_FIELD      = 'text-field'

export const RELATION_FIELD  = 'relation-field'
export const CHECKBOX_FIELD  = 'checkbox-field'
export const RADIO_FIELD     = 'radio-field'
export const SELECT_FIELD    = 'select-field'
export const CASCADER_FIELD  = 'cascader-field'

export const SWITCH_FIELD    = 'switch-field'
export const SLIDER_FIELD    = 'slider-field'
export const RATE_FIELD      = 'rate-field'
export const COLOR_FIELD     = 'color-field'

export const TIME_FIELD      = 'time-field'
export const DATE_FIELD      = 'date-field'

export const UPLOAD_FIELD    = 'upload-field'
export const AVATAR_FIELD    = 'avatar-field'

export const EDITOR_FIELD    = 'editor-field'
export const AUTO_FIELD      = 'auto-field'

export const PAIR_FIELD      = 'pair-field'

export const GROUP_FIELD     = 'group-field'
export const ARRAY_FIELD     = 'array-field'

// 参数定义
/**
 * 预置提示
 */
export const PLACEHOLDER     = 'placeholder'
/**
 * XHtml标签
 */
export const TAG            = 'tag'
/**
 * 选项
 */
export const OPTIONS        = 'options'
/**
 * 表单域创建时动作
 */
export const ON_CREATE      = 'onCreate'

// 表单数据
export const ROW             = 'row'

/**
 * option组件
 * @return VNode[]
 */
function ymOptions(h, _options, fn) {
  let children = []

  if (Array.isArray(_options) && fn) {
    children = _options.map((op) => {
      let ele = fn(op)
      return h(ele.tag, {
        props: {
          ...ele
        },
        attrs: {
          ...ele
        }
      }, ele.text)
    })
  }

  return children
}

const fnSelect = (attr) => ({
  [TAG]: 'el-option',
  ...attr,
  value: attr.value + ''
})

const Select = {
  [TAG]: 'el-select',
  [DFT]: (field) => (field && field.multi) ? [] : null,
  [ON_CREATE]: (h, props) => ymOptions(h, props[OPTIONS], fnSelect)
}

/**
 * 表单域
 * @type { Object.<string, Widget> }
 */
export const Widgets = {
  [AUTO_FIELD]: {
    [LABEL]: '自动选择',
    [ICON]: 'icon-select',
    ...Select,
    filterable: true,
    'allow-create': true,
    'default-first-option': true
  },
  [CASCADER_FIELD]: {
    [TAG]: 'el-cascader',
    [ICON]: 'icon-cascader',
    [LABEL]: '级联菜单',
    [DFT]: () => [],
  },
  [CHECKBOX_FIELD]: {
    [TAG]: 'el-checkbox-group',
    [ICON]: 'icon-checkbox',
    [LABEL]: '多项选择',
    [DFT]: () => [],
    [ON_CREATE]: (h, props) => ymOptions(h, props[OPTIONS], (attr) => ({
      [TAG]: 'el-checkbox',
      label: attr.value,
      text: attr.label
    }))
  },
  [SELECT_FIELD]: {
    [ICON]: 'icon-select',
    [LABEL]: '下拉菜单',
    ...Select,
  },
  [RADIO_FIELD]: {
    [TAG]: 'el-radio-group',
    [ICON]: 'icon-radio1',
    [LABEL]: '单项选择',
    [ON_CREATE]: (h, props) => ymOptions(h, props[OPTIONS], (option) => ({
      [TAG]: 'el-radio',
      ...option,
      label: option.value + '',
      text: option.label
    }))
  },
  [RELATION_FIELD]: {
    [ICON]: 'icon-relation',
    [TAG]: 'ym-relation',
    [LABEL]: '关联'
  },
  [EDITOR_FIELD]: {
    [ICON]: 'icon-editor',
    [LABEL]: '编辑器',
    [TAG]: 'ym-editor'
  },
  [COLOR_FIELD]: {
    [TAG]: 'el-color-picker',
    [ICON]: 'icon-color1',
    [LABEL]: '色彩选择'
  },
  [PAIR_FIELD]: {
    [TAG]: 'ym-pair',
    [ICON]: 'icon-Pair',
    [LABEL]: '选项组',
    [DFT]: () => []
  },
  [UPLOAD_FIELD]: {
    [TAG]: 'ym-file-uploader',
    [ICON]: 'icon-download',
    [LABEL]: '文件上传'
  },
  [DATE_FIELD]: {
    [TAG]: 'el-date-picker',
    [ICON]: 'icon-date1',
    [LABEL]: '日期选择',
  },
  [SLIDER_FIELD]: {
    [TAG]: 'el-slider',
    [ICON]: 'icon-slider',
    [LABEL]: '滑块',
    [DFT]: (field) => (field && field.range) ? [] : null
  },
  [TIME_FIELD]: {
    [TAG]: 'el-time-picker',
    [ICON]: 'icon-time1',
    [LABEL]: '时间选择'
  },
  [SWITCH_FIELD]: {
    [TAG]: 'el-switch',
    [LABEL]: '开关',
    [ICON]: 'icon-switch'
  },
  [RATE_FIELD]: {
    [TAG]: 'el-rate',
    [ICON]: 'icon-rate',
    [LABEL]: '五星评价'
  },
  [INPUT_FIELD]: {
    [TAG]: 'el-input',
    [ICON]: 'icon-input',
    [LABEL]: '定长文本'
  },
  [TEXT_FIELD]: {
    [TAG]: 'el-input',
    [ICON]: 'icon-textarea',
    [LABEL]: '变长文本',
    type: 'textarea',
    block: true,
    autosize: {
      minRows: 2,
      maxRows: 6
    }
  },
  [AVATAR_FIELD]: {
    [TAG]: YmAvatarUploader,
    [ICON]: 'icon-avatar',
    [LABEL]: '照片上传'
  },
  [GROUP_FIELD]: {
    [TAG]: 'ym-group',
    [ICON]: 'el-icon-paperclip',
    [LABEL]: '子表单',
    [FIELDS]: [],
    [DFT]: () => ({})
  },
  [ARRAY_FIELD]: {
    [TAG]: 'ym-array',
    [ICON]: 'el-icon-paperclip',
    [LABEL]: '表单组',
    [FIELDS]: [],
    [DFT]: () => []
  }
}

/**
 * 获取组件默认值
 * @param field 域定义
 * @returns { null|[]|{} }
 */
export function getDefaultValue (field) {
  const d = getValue(field)
  if (d && {}.toString.call(d) === '[object Function]') return d(field)
  return d
}

/**
 * @param {string} widget
 * @return Widget
 */
export function pickWidget (widget) {
  return Widgets[widget] || Widgets[INPUT_FIELD]
}

function getValue (field) {
  if (field[DFT]) return field[DFT]

  if (!field[WIDGET]) return null

  const widget = pickWidget(field[WIDGET])
  return widget[DFT] !== undefined ? widget[DFT] : null
}


