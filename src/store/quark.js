/**
 * 这里定义 QuarkUI 数据
 */

import { LABEL, NAME, WIDGET, DFT, CELL, ICON } from "@/store/elementui";
import {
  INPUT_FIELD,
  SELECT_FIELD,
  OPTIONS,
  TEXT_FIELD,
  GROUP_FIELD,
  COL,
  AUTO_FIELD,
  CHECKBOX_FIELD,
  SLIDER_FIELD,
  PAIR_FIELD,
  PLACEHOLDER,
  FORM,
  WIDTH,
  LABEL_WIDTH,
  LABEL_POSITION,
  RADIO_FIELD,
  RELATION_FIELD,
  SWITCH_FIELD,
  CASCADER_FIELD,
  RATE_FIELD,
  COLOR_FIELD,
  TIME_FIELD,
  DATE_FIELD,
  UPLOAD_FIELD, AVATAR_FIELD, EDITOR_FIELD, Widgets, ARRAY_FIELD, SUBMIT
} from "@/store/form";
import {
  Cells,
  COLUMN_WIDTH,
  COMMON_COLUMN,
  DOWNLOAD_COLUMN,
  ICON_COLUMN,
  PIC_COLUMN,
  TABLE
} from "@/store/table";

/**
 * 布尔
 */
export const BOOL_PASCAL    = 'bool'
/**
 * 整数 { length: <1, 2, 4, 8> }
 */
export const INT_PASCAL     = 'int'
/**
 * 选择框 { options: [], type: <select, radio, checkbox>, multi:bool }
 */
export const SELECT_PASCAL  = 'select'
/**
 * 远程选择 { type: <select, radio, checkbox>, remote: url, multi: bool }
 */
export const REMOTE_PASCAL  = 'remote'
/**
 * char { length: <255 }
 */
export const STRING_PASCAL  = 'string'
/**
 * *char { length }
 */
export const TEXT_PASCAL    = 'text'
/**
 * 日期 { level: <year, month, day> }
 */
export const DATE_PASCAL    = 'date'
/**
 * 时间
 */
export const TIME_PASCAL    = 'time'
/**
 * 浮点数
 */
export const FLOAT_PASCAL   = 'float'
/**
 * 双精度
 */
export const DOUBLE_PASCAL  = 'double'
/**
 * IP地址
 */
export const IP_PASCAL      = 'ip'
/**
 * 文件
 */
export const FILE_PASCAL    = 'file'

const Align = {
  [WIDGET]: SELECT_FIELD,
  [OPTIONS]: [
    {
      label: 'left',
      value: '左'
    },{
      label: 'right',
      value: '右'
    },{
      label: 'center',
      value: '中'
    }
  ]
}

/**
 * @typedef { {} } Pearl
 */

/**
 * 配置项定义
 * @type { Object.<string, Pearl>}
 */
export const Pearls = {
  [NAME]: {
    [NAME]: NAME,
    [LABEL]: '标识符',
    [WIDGET]: INPUT_FIELD,
    rules: [],
    [COL]: 12,
    required: true,
    [DFT]: ''
  },
  [LABEL]: {
    [NAME]: LABEL,
    [LABEL]: '名称',
    [WIDGET]: INPUT_FIELD,
    required: true,
    [COL]: 12,
    [DFT]: ''
  },
  rules: {
    [NAME]: 'rules',
    [LABEL]: '校验',
    [WIDGET]: CHECKBOX_FIELD,
    [OPTIONS]: [
      {
        label: '邮件',
        value: 'email'
      },{
        label: '手机号',
        value: 'phone'
      }, {
        label: '身份证',
        value: 'ID'
      }
    ]
  },
  length: {
    [NAME]: 'length',
    [LABEL]: '文本长度',
    [WIDGET]: INPUT_FIELD,
  },
  [OPTIONS]: {
    [NAME]: OPTIONS,
    [LABEL]: '选项',
    [WIDGET]: PAIR_FIELD
  },
  [PLACEHOLDER]: {
    [NAME]: PLACEHOLDER,
    [LABEL]: '占位文字',
    [WIDGET]: INPUT_FIELD
  },
  [COL]: {
    [NAME]: COL,
    [LABEL]: '表单域排版',
    [WIDGET]: SLIDER_FIELD,
    max: 24,
    min: 1,
    step: 1,
    'show-stops': true,
    showStops: true,
    [DFT]: 24
  },
  [COLUMN_WIDTH]: {
    [NAME]: COLUMN_WIDTH,
    [LABEL]: '表格列宽度(<int>px)',
    [WIDGET]: INPUT_FIELD,
    [COL]: 24
  }
}

function getCommonWidget () {
  return [
    Pearls[COL]
  ]
}

function getCommonCell () {
  return [
    Pearls[COLUMN_WIDTH],
    {
      [LABEL]: '表格内容对齐',
      [NAME]: 'align',
      ...Align,
      [COL]: 12
    }, {
      [LABEL]: '表格标题对齐',
      [NAME]: 'header-align',
      ...Align,
      [COL]: 12
    }
  ]
}

function getCommonComponents () {
  return [
    Pearls[NAME],
    Pearls[LABEL],
    ...getCommonWidget(),
    ...getCommonCell()
  ]
}

/**
 * 组件配置项定义
 * @type { Object.<string, Array<Pearl>>}
 */
export const COMPONENTS = {
  [TABLE]: [
    Pearls[LABEL],
    Pearls[NAME]
  ],
  [ICON_COLUMN]: [
    {
      [NAME]: 'active-icon-class',
      [LABEL]: '打开状态图标的类名',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'inactive-icon-class',
      [LABEL]: '关闭状态图标的类名',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'active-text',
      [LABEL]: '打开状态文字描述',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'inactive-text',
      [LABEL]: '关闭状态文字描述',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },{
      [LABEL]: '关闭状态样式',
      [NAME]: 'inactive-style',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },{
      [LABEL]: '打开状态样式',
      [NAME]: 'active-style',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    }
  ],
  [PIC_COLUMN]: [
    {
      [LABEL]: '高度',
      [NAME]: 'height',
      [WIDGET]: INPUT_FIELD
    },{
      [LABEL]: '宽度',
      [NAME]: 'width',
      [WIDGET]: INPUT_FIELD
    },{
      [LABEL]: '适配',
      [NAME]: 'fit',
      [WIDGET]: SELECT_FIELD,
      // /  /  /  /
      [OPTIONS]: [
        {
          label: '铺满',
          name: 'fill'
        },{
          label: '缩放',
          name: 'contain'
        },{
          label: '剪裁',
          name: 'cover'
        },{
          label: '原样',
          name: 'none'
        },{
          label: '最小',
          name: 'scale-down'
        }
      ]
    }
  ],
  [DOWNLOAD_COLUMN]: [
    {
      [NAME]: 'relation',
      [LABEL]: 'url字段名',
      [WIDGET]: INPUT_FIELD,
      [DFT]: 'url'
    }
  ],
  [FORM]: [
    Pearls[NAME],
    Pearls[LABEL],
    // Fields 通过拖拽处理
    // Toolbar 计划通过拖拽处理，主要是清空按钮的处理
    {
      [LABEL]: '宽度',
      [NAME]: WIDTH,
      [WIDGET]: INPUT_FIELD
    },
    {
      [LABEL]: '标签宽度',
      [NAME]: LABEL_WIDTH,
      [WIDGET]: INPUT_FIELD
    },
    {
      [LABEL]: '标签位置',
      [NAME]: LABEL_POSITION,
      [WIDGET]: RADIO_FIELD,
      [OPTIONS]: [
        {
          label: '左',
          value: 'left'
        },
        {
          label: '右',
          value: 'right'
        },
        {
          label: '上',
          value: 'top'
        },
        {
          label: '无',
          value: 'none'
        }
      ]
    },
    {
      [LABEL]: '提交地址',
      [NAME]: SUBMIT,
      [WIDGET]: INPUT_FIELD
    }
  ],
  [GROUP_FIELD]: [
  ],
  [ARRAY_FIELD]: [

  ],
  [RELATION_FIELD]: [
    {
      [LABEL]: '关联地址',
      [NAME]: 'url',
      [WIDGET]: INPUT_FIELD
    }
  ],
  [INPUT_FIELD]: [
    // {
    //   [NAME]: 'pascal',
    //   [LABEL]: '数据类型',
    //   [WIDGET]: SELECT_FIELD,
    //   [OPTIONS]: [
    //     {
    //       label: '文本',
    //       value: 'string'
    //     },
    //     {
    //       label: '整数',
    //       value: 'int'
    //     },
    //     {
    //       label: '小数',
    //       value: 'float'
    //     }
    //   ],
    //   [DFT]: 'string'
    // },
  ],
  [TEXT_FIELD]: [
  ],
  [AUTO_FIELD]: [
    Pearls[OPTIONS],
    {
      [NAME]: 'multi',
      [LABEL]: '多选',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false
    },
    {
      [NAME]: 'filterable',
      [LABEL]: '启用搜索',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false,
      when: {
        name: WIDGET,
        value: SELECT_FIELD
      }
    },
    {
      [NAME]: 'remote',
      [LABEL]: '远程搜索地址',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      when: {
        name: WIDGET,
        value: SELECT_FIELD
      }
    }
  ],
  [RADIO_FIELD]: [
    Pearls[OPTIONS]
  ],
  [CHECKBOX_FIELD]: [
    Pearls[OPTIONS]
  ],
  [SELECT_FIELD]: [
    Pearls[OPTIONS],
    {
      [NAME]: 'multi',
      [LABEL]: '多选',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false,
      [COL]: 12
    },
    {
      [NAME]: 'filterable',
      [LABEL]: '启用搜索',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false,
      [COL]: 12
    },
    {
      [NAME]: 'remote',
      [LABEL]: '远程搜索地址',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      when: {
        name: 'filterable',
        value: true
      }
    }
  ],
  [CASCADER_FIELD]: [
    Pearls[OPTIONS]
  ],
  [SWITCH_FIELD]: [
    {
      [NAME]: 'active-icon-class',
      [LABEL]: '打开状态图标的类名',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'inactive-icon-class',
      [LABEL]: '关闭状态图标的类名',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'active-text',
      [LABEL]: '打开状态文字描述',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'inactive-text',
      [LABEL]: '关闭状态文字描述',
      [WIDGET]: INPUT_FIELD,
      [DFT]: null,
      [COL]: 12
    }
  ],
  [SLIDER_FIELD]: [
    {
      [NAME]: 'min',
      [LABEL]: '最小值',
      [DFT]: 0,
      [WIDGET]: INPUT_FIELD,
      translate: v => parseInt(v),
      [COL]: 8
    },
    {
      [NAME]: 'max',
      [LABEL]: '最大值',
      [WIDGET]: INPUT_FIELD,
      translate: v => parseInt(v),
      [DFT]: 50,
      [COL]: 8
    },
    {
      [NAME]: 'step',
      [LABEL]: '步长',
      [WIDGET]: INPUT_FIELD,
      translate: v => parseInt(v),
      [DFT]: 1,
      [COL]: 8
    },
    {
      [NAME]: 'showStops',
      [LABEL]: '显示间断点',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false,
      [COL]: 12
    },
    {
      [NAME]: 'range',
      [LABEL]: '范围选择',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false,
      [COL]: 12
    }
  ],
  [RATE_FIELD]: [
  ],
  [PAIR_FIELD]: [
  ],
  [COLOR_FIELD]: [
    {
      [NAME]: 'show-alpha',
      [LABEL]: '透明度',
      [WIDGET]: SWITCH_FIELD,
      [DFT]: false
    }
  ],
  [TIME_FIELD]: [
  ],
  [DATE_FIELD]: [
    {
      [NAME]: 'type',
      [LABEL]: '类型',
      [WIDGET]: SELECT_FIELD,
      [OPTIONS]: [
        {
          label: '年',
          value: 'year'
        },
        {
          label: '月',
          value: 'month'
        },
        {
          label: '日',
          value: 'date'
        },
        {
          label: '时',
          value: 'time'
        }
      ]
    }
  ],
  [UPLOAD_FIELD]: [
    {
      [NAME]: 'url',
      [LABEL]: '上传地址',
      [WIDGET]: INPUT_FIELD,
      required: true,
      [DFT]: null
    },
    {
      [NAME]: 'multiple',
      [WIDGET]: SWITCH_FIELD,
      [LABEL]: '多选',
      [DFT]: false,
      [COL]: 12
    },
    {
      [NAME]: 'limit',
      [WIDGET]: INPUT_FIELD,
      'when': {
        name: 'multiple',
        value: true
      },
      [LABEL]: '个数限制',
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'complete-label',
      [LABEL]: '完成文字',
      [WIDGET]: INPUT_FIELD,
      required: true,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'preload-label',
      [LABEL]: '按钮标题',
      [WIDGET]: INPUT_FIELD,
      required: true,
      [DFT]: null,
      [COL]: 12
    },
    {
      [NAME]: 'accept',
      [WIDGET]: INPUT_FIELD,
      [LABEL]: '类型限制',
      [DFT]: null
    }
  ],
  [AVATAR_FIELD]: [
    {
      [NAME]: 'url',
      [LABEL]: '上传地址',
      [WIDGET]: INPUT_FIELD,
      required: true,
      [DFT]: null
    },
    {
      [NAME]: 'width',
      [LABEL]: '图片宽px',
      [WIDGET]: INPUT_FIELD,
      [DFT]: '178px'
    },
    {
      [NAME]: 'height',
      [LABEL]: '图片高px',
      [WIDGET]: INPUT_FIELD,
      [DFT]: '178px'
    }
  ],
  [EDITOR_FIELD]: [
  ],
  [BOOL_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(SWITCH_FIELD),
    selectCell(ICON_COLUMN)
  ],
  [INT_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(SLIDER_FIELD),
    selectCell()
  ],
  [FLOAT_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(),
    selectCell()
  ],
  [DOUBLE_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(),
    selectCell()
  ],
  [TIME_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(TIME_FIELD),
    selectCell()
  ],
  [DATE_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(DATE_FIELD),
    selectCell()
  ],
  [SELECT_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(SELECT_FIELD),
    selectCell()
  ],
  [REMOTE_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(RELATION_FIELD),
    selectCell()
  ],
  [IP_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(),
    selectCell()
  ],
  [FILE_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(UPLOAD_FIELD),
    selectCell()
  ],
  [STRING_PASCAL]: [
    ...getCommonComponents(),
    Pearls.length,
    Pearls.rules,
    selectWidget(),
    selectCell(),
    {
      [NAME]: 'type',
      [LABEL]: '文本框类型',
      [WIDGET]: SELECT_FIELD,
      [OPTIONS]: [
        {
          label: '文本',
          value: 'text'
        },
        {
          label: '密码',
          value: 'password'
        }
      ],
      [DFT]: 'text',
      [COL]: 12
    },
    {
      ...Pearls.length,
      [COL]: 12
    },
    Pearls.rules,
    Pearls.placeholder,
  ],
  [TEXT_PASCAL]: [
    ...getCommonComponents(),
    selectWidget(TEXT_FIELD),
    selectCell()
  ]
}

/**
 * @typedef { object } Quark
 *
 * @property { string } [WIDGET] 默认表单域组件名称
 * @property { string } [CELL] 默认表格列单元格显示器名称
 *
 * @property { string } [LABEL]
 * @property { string } [ICON]
 */

/**
 * @type { Object.<string, Pearl> }
 */
export const Quarks     = {
  [BOOL_PASCAL]: {
    [WIDGET]: SWITCH_FIELD,
    [CELL]: ICON_COLUMN,
    [LABEL]: '布尔值',
    [ICON]: 'el-icon-switch-button'
  },
  [INT_PASCAL]: {
    [WIDGET]: SLIDER_FIELD,
    [LABEL]: '整数',
    'unsigned': false,
    [ICON]: 'el-icon-s-operation',
  },
  [FLOAT_PASCAL]: {
    [WIDGET]: INPUT_FIELD,
    [LABEL]: '单精度数',
    [ICON]: 'el-icon-bicycle'
  },
  [DOUBLE_PASCAL]: {
    [WIDGET]: INPUT_FIELD,
    [LABEL]: '双精度数',
    [ICON]: 'el-icon-truck'
  },
  [TIME_PASCAL]: {
    [WIDGET]: TIME_FIELD,
    [LABEL]: '时间',
    [ICON]: 'el-icon-timer'
  },
  [DATE_PASCAL]: {
    [WIDGET]: DATE_FIELD,
    [LABEL]: '日期',
    [ICON]: 'el-icon-date'
  },
  [SELECT_PASCAL]: {
    [WIDGET]: SELECT_FIELD,
    [LABEL]: '枚举',
    [ICON]: 'el-icon-thumb'
  },
  [REMOTE_PASCAL]: {
    [WIDGET]: RELATION_FIELD,
    [LABEL]: '字典',
    [ICON]: 'el-icon-connection'
  },
  [IP_PASCAL]: {
    [WIDGET]: INPUT_FIELD,
    rules: ['ip'],
    [LABEL]: 'IP地址',
    [ICON]: 'el-icon-paperclip'
  },
  [FILE_PASCAL]: {
    [WIDGET]: UPLOAD_FIELD,
    [LABEL]: '文件',
    [ICON]: 'el-icon-upload'
  },
  [STRING_PASCAL]: {
    [WIDGET]: INPUT_FIELD,
    [LABEL]: '文本',
    [ICON]: 'el-icon-house'
  },
  [TEXT_PASCAL]: {
    [WIDGET]: TEXT_FIELD,
    [LABEL]: '段落',
    [ICON]: 'el-icon-tickets'
  }
}

function getOptionsOf (obj) {
  return Object.keys(obj).map(key => ({
    label: obj[key][LABEL],
    value: obj[key][WIDGET] || key
  }))
}

function selectCell (d = COMMON_COLUMN) {
  return {
    [NAME]: CELL,
    [LABEL]: '表格内容显示样式',
    [WIDGET]: SELECT_FIELD,
    [OPTIONS]: getOptionsOf(Cells),
    [DFT]: d,
    [COL]: 12
  }
}

function selectWidget ( d= INPUT_FIELD ) {
  return {
    [NAME]: WIDGET,
    [LABEL]: '表单域组件类型',
    [WIDGET]: SELECT_FIELD,
    [OPTIONS]: getOptionsOf(Widgets),
    [DFT]: d,
    [COL]: 12
  }
}
