
/**
 * @typedef { object } ElementUI~View
 *
 * @property { string } [TITLE] 视图标题
 * @property { {} } [DEFINITION] 视图定义
 */

/**
 * 视图定义
 */
export const VIEW       = 'view';

/**
 * 主页面
 */
export const MAIN_VIEW  = 'main';

/**
 * 表单视图
 */
export const FORM_VIEW  = 'form';
/**
 * 图表视图
 */
export const CHART_VIEW = 'chart';
/**
 * 表格视图
 */
export const CURD_VIEW  = 'curd';
/**
 * 单表格视图
 */
export const TABLE_VIEW = 'table';
/**
 * 构建视图
 */
export const MAKER_VIEW = 'maker';
/**
 * 菜单视图
 */
export const MENU_VIEW  = 'menu';

/**
 * @type { Object.<string, string> }
 */
export const Views = {
  [MAIN_VIEW]:  'YmMain',
  [FORM_VIEW]:  'YmFormView',
  [CURD_VIEW]:  'YmCurd',
  [MENU_VIEW]:  'YmMenu',
  [TABLE_VIEW]: 'YmTable',
  [CHART_VIEW]: 'YmChart',
  [MAKER_VIEW]: 'YmMaker'
}
