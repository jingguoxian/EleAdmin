
// 表格参数定义
import { ICON, LABEL } from "@/store/elementui"

/**
 * @typedef { object } Cell
 *
 * @property { string } [LABEL]
 * @property { string } [ICON]
 */

/**
 * @typedef { object } Column
 *
 * @property { string } [LABEL]
 * @property { string } [NAME]
 *
 * @property { string } [COLUMN_WIDTH] npx 列宽
 * @property { ICON_COLUMN | PIC_COLUMN | DOWNLOAD_COLUMN |COMMON_COLUMN } [CELL] 显示定义
 * @property { "center"|"left"|"right" } header-align 表头对齐
 * @property { "center"|"left"|"right" } align 内容对齐
 *
 * @property { boolean } [sortable] 可页内排序
 * @property { boolean } [resizable] 列宽可调整
 */

/**
 *
 * @typedef { object } Table
 *
 * @property { Array<Column> } [COLUMNS] 表格列列表
 * @property {{ where: [], options: Array<{value, label: string, name: string}> }} [SEARCH] 查询单元定义
 * @property { boolean | Column } [SELECTABLE] 选择列定义
 * @property { Array<ElementUI~Button> } [EMBEDDED] 行操作按钮列表
 *
 * @property { {total: number, "page-size": number, "current-page": number} } [PAGINATION] 分页数据
 * @property { array<{}> } [rows]
 * @property { array<number> } [SELECTED] 选中行列表
 */

export const TABLE        = 'table'
/**
 * 列表(array) {name; label; width; sortable; display}
 */
export const COLUMNS    = 'columns';
/**
 * 查询参数(object) { where; elements }; { OPTIONS; FIELDS }
 */
export const SEARCH     = 'search';
/**
 * 可选择(object)
 */
export const SELECTABLE = 'selectable'
/**
 * 列宽
 */
export const COLUMN_WIDTH = 'width'

// 列参数定义
/**
 * 表格显示定义(string) 在UI中定义
 */
// export const CELL    = 'cell';
/**
 * 行操作对象(object)
 *   actions.items
 */
export const EMBEDDED   = 'embedded';
/**
 * 表格列可排序(boolean)
 */
// export const SORTABLE   = 'sortable';

// 表格数据定义
/**
 * 预选择(array)
 */
export const SELECTED   = 'selected'
/**
 * 分页(object)
 */
export const PAGINATION = 'pagination'
/**
 * 数据 (array)
 */
export const ROWS         = 'rows'

/**
 * 列样式
 */
export const ICON_COLUMN     = 'icon'
export const PIC_COLUMN      = 'pic'
export const DOWNLOAD_COLUMN = 'download'
export const COMMON_COLUMN   = 'common'

/**
 * @type { Object.<string, Cell> }
 */
export const Cells = {
  [ICON_COLUMN]: {
    [LABEL]: '图标',
    [ICON]: 'el-icon-info'
  },
  [PIC_COLUMN]: {
    [LABEL]: '图片',
    [ICON]: 'el-icon-picture'
  },
  [DOWNLOAD_COLUMN]: {
    [LABEL]: '链接',
    [ICON]: 'el-icon-link'
  },
  [COMMON_COLUMN]: {
    [LABEL]: '普通',
    [ICON]: 'el-icon-minus'
  }
}



