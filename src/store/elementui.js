/**
 * ElementUI 定义文件
 */
/**
 * @typedef { object } ElementUI
 */
/**
 * 视图标题(string)
 */
export const TITLE      = 'title';
/**
 * 标签(string)
 */
export const LABEL      = 'label';
/**
 * 名称(string)
 */
export const NAME       = 'name';
/**
 * 默认值(string)
 */
export const DFT        = 'default';
/**
 * (string)
 */
export const PASCAL     = 'pascal';
/**
 * (object) 定义
 */
export const DEFINITION = 'definition';
/**
 * 组件
 */
export const WIDGET     = 'widget'

/**
 * 操作组(object)
 *   toolbar.items
 */
export const TOOLBAR = 'toolbar';

/**
 * 按钮表(array) {label}
 */
export const ITEMS = 'items';


/**
 * (string)  /xxx/xxx
 */
export const URL = 'url';

/**
 * 前端显示样式名称(string)
 */
export const CELL = 'cell';

/**
 * 前台URL负载参数(bool|object)
 */
export const PAYLOAD = 'payload';

/**
 * 跳转URL {path; layer} | url
 */
export const REDIRECT = 'redirect';

/**
 * 组件图标
 */
export const ICON = 'icon'

/**
 * 值(mixed)
 */
export const VALUE = 'value';

/**
 * 组件索引
 */
export const INDEX = '__$INDEX$__'
