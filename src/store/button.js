import {ICON, LABEL, NAME, PAYLOAD} from "@/store/elementui";


export const BUTTON = 'button'

export const SUBMIT_BUTTON = 'submit'

export const ADD_BUTTON = 'add-button'

export const REMOVE_BUTTON = 'remove-button'

export const COMMON_BUTTON = 'common-button'

export const EDIT_BUTTON = 'edit-button'

export const INTENT = 'intent'

export const DIALOG = 'dialog'

export const TYPE = 'type'

export const BTN_PRIMARY = 'primary'

export const BTN_SUCCESS = 'success'

export const BTN_DANGER = 'danger'

export const CONFIRM = 'confirm'

/**
 * @typedef { object } ElementUI~Button
 *
 * @property { string } [NAME] 按钮预定义
 * @property { string | { [module]: string, [controller]: string, [action]: string } } [INTENT] 按钮关联的意图
 * @property { string } [ICON] 按钮图标
 * @property { BTN_PRIMARY|BTN_SUCCESS|BTN_DANGER } [TYPE] 按钮显示类型
 * @property { string } url 按钮提交URL
 * @property { boolean | string | {} } [payload]负载
 * @property { string | {} } [CONFIRM] 提示
 *
 * @property { string } [DIALOG] 弹出对话框标题<1>
 *
 * @property { {} } [file] 文件上传按钮参数<2>
 *
 * @property { string } [switch] 切换按钮绑定的字段名<3>
 * @property { boolean } [icon-only] 图标按钮
 *
 * @property { {} } [row] 按钮绑定的对象，与 switch 关联, 与 dialog 关联, 与 url 关联
 *
 */

/**
 * @typedef { object } ElementUI~Toolbar
 *
 * @property { Array<ElementUI~Button> } items 按钮列表
 *
 * @property { {} } [row] 工具栏绑定的数据
 */

/**
 * 常用按钮定义按钮
 * @type { Object.<string, ElementUI~Button> }
 */
export const Buttons = {
  [SUBMIT_BUTTON]: {
    [ICON]: 'el-icon-ok',
    [LABEL]: '提交',
    [TYPE]: BTN_SUCCESS
  },
  [ADD_BUTTON]: {
    [NAME]: 'add',
    [INTENT]: 'add',
    [ICON]: 'el-icon-add',
    [LABEL]: '新增',
    [PAYLOAD]: false,
    [DIALOG]: true,
    [TYPE]: BTN_PRIMARY
  },
  [EDIT_BUTTON]: {
    [NAME]: 'edit',
    [INTENT]: 'edit',
    [ICON]: 'el-icon-edit',
    [LABEL]: '修改',
    [PAYLOAD]: true,
    [DIALOG]: true,
    [TYPE]: BTN_SUCCESS
  },
  [REMOVE_BUTTON]: {
    [NAME]: 'remove',
    [INTENT]: 'delete',
    [ICON]: 'el-icon-delete',
    [LABEL]: '删除',
    [CONFIRM]: '确定要删除？',
    [PAYLOAD]: true,
    [TYPE]: BTN_DANGER
  },
  [COMMON_BUTTON]: {
  }
}
