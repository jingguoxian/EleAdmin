//const Ui = require('../store/elementui')
//import * as Ui from '@/store/elementui'

const mocker = (app) => {
  app.get('/index/element/index', (req, res) => {
    res.json({
        view: 'main',
        definition: {
          apis: {
            menus: '/index/element/menus',
            departments: '/index/element/departments'
          }
        }
      }
    );
  })
  app.get('/index/element/departments', (req, res) => {
    res.json([]);
  })
  app.get('/index/element/menus', (req, res) => {
    res.json([
      {
        route: { path: '/index/table/index' },
        label: '高级表格',
        icon: 'el-icon-office-building',
        id: 1
      },
      {
        label: "高级表单",
        id: 3,
        icon: 'el-icon-postcard',
        route: { path: '/index/form/edit' }
      },
      {
        label: '图表示例',
        id: 6,
        icon: 'el-icon-bell',
        items: [{
          route: { path: '/index/chart/circle' },
          label: '单一图表',
          id: 40
        }, {
          route: { path: '/index/chart/big' },
          label: '数字大屏',
          id: 50
        }]
      },
      {
        route: { path: '/index/maker/module' },
        label: '模型设计',
        id: 60
      }]
    );
  })
  app.get('/index/table/index', (req, res) => {
    res.json({
      view: 'curd',
      definition: {
        title: '用户管理',
        url: '/index/table/rows',
        columns: [
          {
            name: 'name', label: '姓名', align: 'center', width: 200,
            display: {
              widget: 'url',
              relation: 'url'
            }
          }, {
            name: 'age', label: '年龄', align: 'center', width: 100, sortable: true
          }, {
            name: 'sex', label: '性别', align: 'center', width: 100,
            display: {
              widget: 'bool',
              texts: ['女', '男'],
              icons: ['el-icon-female', 'el-icon-male'],
              styles: [{
                color: '#F56C6C'
              }, {
                color: '#409EFF'
              }]
            }
          }, {
            name: 'address', label: '地址', align: 'left', 'header-align': 'center'
          }
        ],
        embedded: {
          items: [
            {
              type: 'success',
              label: '修改',
              redirect: {
                url: 'index/form/edit'
              },
              payload: true
            },{
              type: 'danger',
              label: '删除',
              confirm: '确定要删除？'
            }
          ]
        },
        search: {
          options: [
            {
              label: '姓名',
              name: 'name'
            }, {
              label: '年龄',
              name: 'age'
            }
          ]
        },
        toolbar: {
          items: [
            {
              label: '新建',
              type: 'primary',
              dialog: true,
              title: '新增用户',
              url: 'index/form/add'
            }, {
              label: '删除',
              type: 'danger',
              diskey: 'selected',
              payload: true,
              url: '/index/api/delete'
            }
          ]
        }
      }
    })
  })
  app.post('/index/table/rows', (req, res) => {
    res.json({
      rows: [
        {
          id: 1,
          name: '张山',
          age: 30,
          sex: 1,
          address: '一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库',
          url: 'index/form/edit/id/1'
        }, {
          id: 2,
          name: '吴家',
          sex: 0,
          age: 28,
          address: '需求分析 当我们去实现一个组件库的时候',
          url: 'index/form/edit/id/2'
        }
      ]
    });
  })
  app.get('/index/form/edit', (req, res) => {
    res.json({
      view: 'form',
      definition: {
        title: '修改用户信息',
        fields: [
          {
            name: 'name', label: '名称', widget: 'input', col: 12
          }, {
            name: 'age', label: '年龄', widget: 'slider', col: 12
          }, {
            name: 'address', label: '地址', widget: 'input'
          }
        ],
        width: '800px',
        url: '/index/form/row',
        submit: '/index/form/edit'
      }
    })
  })
  app.get('/index/form/add', (req, res) => {
    res.json({
      view: 'form',
      definition: {
        // title: '新增用户',
        fields: [
          {
            name: 'name', label: '名称', widget: 'input', col: 12
          }, {
            name: 'age', label: '年龄', widget: 'slider', col: 12
          }, {
            name: 'address', label: '地址', widget: 'input'
          }
        ],
        width: '800px',
        submit: '/index/form/add'
      }
    })
  })
  app.post('/index/form/add', (req, res) => {
    res.json({
      message: {
        text: '新增完成',
        type: 'success'
      }
    })
  })
  app.post('/index/form/edit', (req, res) => {
    res.json({
      message: {
        text: '修改成功',
        type: 'success'
      },
      redirect: '/index/table/index'
    })
  })
  app.get('/index/form/row', (req, res) => {
    res.json({ row: {
      'name': '张山',
      'age': 30,
      'address': '一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的桌面端组件库'
    } })
  })
  app.get('/index/api/delete', (req, res) => {
    res.json({
      text: '已删除',
      type: 'success'
    })
  })
  app.get('/index/maker/module', (req, res) => {
    res.json({
      view: 'maker',
      definition: {
        //
      }
    });
  })
  app.get('/index/api/options', (req, res) => {
    res.json([{
      label: '张三',
      value: 1
    },{
      label: '李四',
      value: 2
    }])
  })
}

// export default mocker
module.exports = mocker
