# ele-admin

感谢关注！

经过这段时间的完善，预想的功能基本上实现了。

这套东西主要是基于elementUI实现的一个*前后端分离系统*的**前端**

* 后端发送一个Json，前端自动生成表单、表格、图表、按钮、对话框
* 有Json生成器，可通过拖拽的方式自己设计
* 表格实现了icon列、url跳转列、下载列
* chart使用EChart

## 效果演示

表格

![表格效果](https://images.gitee.com/uploads/images/2021/0309/120340_dd5d51d3_1472435.png "表格.png")

表单

![表单](https://images.gitee.com/uploads/images/2021/0309/120424_d5e008fb_1472435.png "表单.png")

对话框表单

![对话框表单](https://images.gitee.com/uploads/images/2021/0309/120442_6e2a623a_1472435.png "对话框.png")

表单生成器

![生成器](https://images.gitee.com/uploads/images/2021/0309/120516_1ae226e5_1472435.png "生成.png")

配置

![配置](https://images.gitee.com/uploads/images/2021/0309/120538_0e6a5bda_1472435.png "配置.png")

## Json实例

下面的Json将渲染一个复杂的表格：
```javascript
return {
      view: 'curd',
      definition: {
        title: '用户管理',
        url: '/index/table/rows',
        columns: [
          {
            name: 'name', label: '姓名', align: 'center', width: 200,
            display: {
              widget: 'url',
              relation: 'url'
            }
          }, {
            name: 'age', label: '年龄', align: 'center', width: 100, sortable: true
          }, {
            name: 'sex', label: '性别', align: 'center', width: 100,
            display: {
              widget: 'bool',
              texts: ['女', '男'],
              icons: ['el-icon-female', 'el-icon-male'],
              styles: [{
                color: '#F56C6C'
              }, {
                color: '#409EFF'
              }]
            }
          }, {
            name: 'address', label: '地址', align: 'left', 'header-align': 'center'
          }
        ],
        embedded: {
          items: [
            {
              type: 'success',
              label: '修改',
              redirect: {
                url: 'index/form/edit'
              },
              payload: true
            },{
              type: 'danger',
              label: '删除',
              payload: true,
              confirm: '确定要删除？'
            }
          ]
        },
        search: {
          options: [
            {
              label: '姓名',
              name: 'name'
            }, {
              label: '年龄',
              name: 'age'
            }
          ]
        },
        toolbar: {
          items: [
            {
              label: '新建',
              type: 'primary',
              dialog: true,
              title: '新增用户',
              url: 'index/form/add'
            }, {
              label: '删除',
              type: 'danger',
              diskey: 'selected',
              payload: true,
              url: '/index/api/delete'
            }
          ]
        }
      }
    }
```

下面Json渲染一个表单

```javascript
return {
      view: 'form',
      definition: {
        // title: '新增用户',
        fields: [
          {
            name: 'name', label: '名称', widget: 'input', col: 12
          }, {
            name: 'age', label: '年龄', widget: 'slider', col: 12
          }, {
            name: 'address', label: '地址', widget: 'input'
          }
        ],
        width: '800px',
        submit: '/index/form/add'
      }
    }
```

elementui的表单域基本都完成了。复杂一点的，你想自定义表单域。看这个SWITCH示例。

```javascript
return [
    Pearls[LABEL],
    getCommonForm([
        {
            [NAME]: 'active-icon-class',
            [LABEL]: '打开状态图标的类名',
            [WIDGET]: INPUT_FIELD,
            [DFT]: null
        },
        {
            [NAME]: 'inactive-icon-class',
            [LABEL]: '关闭状态图标的类名',
            [WIDGET]: INPUT_FIELD,
            [DFT]: null
        },
        {
            [NAME]: 'active-text',
            [LABEL]: '打开状态文字描述',
            [WIDGET]: INPUT_FIELD,
            [DFT]: null
        },
        {
            [NAME]: 'inactive-icon-class',
            [LABEL]: '关闭状态文字描述',
            [WIDGET]: INPUT_FIELD,
            [DFT]: null
        }
    ])
]
```
## 安装
```
yarn install
```

### 调试
```
yarn serve
```

### 构建
```
yarn build
```

### 检查
```
yarn lint
```
